﻿using Sl_Processo.Filters;
using Sl_Processo.Models;
using Sl_Processo.Models.ValueObjects;
using Sl_Processo.Repositorio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Sl_Processo.Controllers
{
    [Autenticacao]
    public class LicitacoesController : Controller
    {
        ProcessoRepositorio rProcesso = new ProcessoRepositorio();
        LicitacoesRepositorio rLicitacoes = new LicitacoesRepositorio();

        public ActionResult Index()
        {
            ViewBag.Anos = new SelectList(ListaAnos(), "value", "Text");
            ViewBag.CodUnidadeInterna = new SelectList(rProcesso.ListaEquipes(), "CodUnidadeInterna", "NomeUnidadeInterna");

            return View();
        }
        public ActionResult Lista(LicitacaoVO licitacao)
        {
            IEnumerable<VW_ResultadoPrincipalLicitacao> lista = new List<VW_ResultadoPrincipalLicitacao>();

            if (licitacao.Ano == 0 && licitacao.Equipe == 0 && string.IsNullOrEmpty(licitacao.Processo))
            {
                lista = rLicitacoes.BuscarTodos();
                return View(lista);
            }
            else
            {
                lista = rLicitacoes.ListaProcessosCriterio(licitacao);
                return View(lista);
            }
            
        }
        protected IEnumerable<object> ListaAnos()
        {
            List<SelectListItem> Anos = new List<SelectListItem>();

            for (int i = 0; i < 3; i++)
            {
                int ano = (DateTime.Now.Year - 1) + i;
                string anotext = ano.ToString();
                Anos.Add(new SelectListItem { Text = anotext, Value = anotext.ToString() });
            }

            return Anos;
        }
    }
}