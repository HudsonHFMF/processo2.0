﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sl_Processo.Models;
using System.Data.SqlClient;
using System.Configuration;
using Sl_Processo.Filters;


namespace Sl_Processo.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly Context model = new Context();
       

        [AllowAnonymous]
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult PartialLogin()
        {          
          return View();
        }

        [HttpPost]
        public ActionResult Login(string login, string senha)
        {
            var user = Autenticacao.BuscarUsuario(login, senha);

            if (user != null)
            {
                Session["user"] = user;
                Session["username"] = user.nome;
                Session["codUndInterna"] = user.TB_UnidadeInterna.CodUnidadeInterna;
                Session["setorSigla"] = user.TB_UnidadeInterna.NomeUnidadeInterna;

                Session["erroLogin"] = null;

                return RedirectToAction("IndexRelatorios", "Home");
               
            }
            else
            {
                Session["erroLogin"] = "C.P.F ou Senha Inválida!";
                return RedirectToAction("Index");
            }
           
        }
        
        public ActionResult Logoff ()
        {
            Session["user"] = null;
            Session["username"] = null;
            Session["codUndInterna"] = null;
            Session["setorSigla"] = null;
            Session.Abandon();
            return RedirectToAction("Index");
        }       
      
    }
}