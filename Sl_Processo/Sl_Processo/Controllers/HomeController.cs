﻿using System.Web.Mvc;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;


namespace Sl_Processo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
          

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("chart")
                .SetXAxis(new XAxis
                {
                    Categories = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }
                })
                .SetSeries(new Series
                {
                    Data = new Data(new object[] { 29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4 })
                });

            return View(chart);
        }

        public ActionResult IndexRelatorios() {
            ViewBag.Setor = Session["codUndInterna"];
            return View();
        }

       

    }
}