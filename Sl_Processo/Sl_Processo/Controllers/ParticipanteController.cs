﻿using Sl_Processo.Filters;
using Sl_Processo.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sl_Processo.Controllers
{
    [Autenticacao]
    public class ParticipanteController : Controller
    {
        EmpresaParticipanteRepositorio rParticipante = new EmpresaParticipanteRepositorio();
        // GET: Participante
        public ActionResult Index(string letra="A")
        {
            return View(rParticipante.BuscarTodos(letra));
        }
    }
}