﻿using Sl_Processo.Repositorio;
using Sl_Processo.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Sl_Processo.Filters;
using Sl_Processo.Models.ValueObjects;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using System.Drawing;
using Point = DotNet.Highcharts.Options.Point;

namespace Sl_Processo.Controllers
{
    [Autenticacao]
    public class ProcessoController : Controller
    {
        ProcessoRepositorio rProcesso = new ProcessoRepositorio();
        MovimentoProcessoRepositorio rMovimentoProcesso = new MovimentoProcessoRepositorio();
        ControleDeProcessosRepositorio rControleProcesso = new ControleDeProcessosRepositorio();
        EmpresaParticipanteRepositorio rParticipante = new EmpresaParticipanteRepositorio();
        UnidadeInternaRepositorio rUnidadeInterna = new UnidadeInternaRepositorio();

        int codSetor = 0;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ConsultaProcessosDoParticipante()
        {
            //ViewBag.CodParticipante = new SelectList(rParticipante.BuscarTodos(), "CodParticipante", "RazaoSocial");
            return View();
        }
        public ActionResult ProcessosDoParticipante(int codParticipante)
        {
            var objParticipante = rParticipante.BuscarParticipantePorCodigo(codParticipante);
            ViewBag.participanteCnpj = objParticipante.CNPJFormatado;
            ViewBag.participanteNome = objParticipante.RazaoSocial;
            return View(rProcesso.BuscarProcessosDoParticipanteNoPeriodo(codParticipante));
        }
        public ActionResult Historico(TB_Processo processo)
        {
            if (string.IsNullOrEmpty(processo.NumeroProcesso) && string.IsNullOrEmpty(processo.Objeto))
            {
                ViewBag.setClass = "alert alert-danger";
                ViewBag.message = "Atenção !!! Informe pelo menos um parâmetro para a pesquisa";
                return View("Index");
            }

            try
            {
                var numeroProcesso = processo.NumeroProcesso;
                var objetoProcesso = processo.Objeto == null ? string.Empty : processo.Objeto;

                if (objetoProcesso.Length > 0)
                {
                    return RedirectToAction("ListaDeProcessosPorAssunto", processo);
                }
                var objProcesso = rProcesso.BuscarProcessoPorNumeroProcesso(processo);

                return View(objProcesso);
            }
            catch (Exception)
            {
                ViewBag.ProcessoNaoLocalizado = "Número de Processo Não Localizado!";
                return View("Index");
            }
           
        }
        public ActionResult ListaDeProcessosPorAssunto(TB_Processo processo)
        {
            var lista = rProcesso.BuscarProcessoPorAssunto(processo);
            return View(lista);
        }
        public ActionResult PrazosEquipes()
        {
            codSetor = Convert.ToInt32(Session["codUndInterna"]);
            ViewBag.Setor = codSetor;

            ViewBag.Anos = new SelectList(ListaAnos(), "value", "Text");

            if (Administrador(codSetor))
            {
                ViewBag.CodUnidadeInterna = new SelectList(rProcesso.ListaEquipes(), "CodUnidadeInterna", "NomeUnidadeInterna");
            }
            else
            {
                ViewBag.CodUnidadeInterna = new SelectList(rProcesso.BuscarUnidadePorId(codSetor), "CodUnidadeInterna", "NomeUnidadeInterna", codSetor);
            }
            ViewBag.CodDescricaoMovimento = new SelectList(rProcesso.ListaDescricaoMovimentacao(), "CodDescricaoMovimento", "DescricaoMovimento");
            return View();
        }
        [HttpPost]
        public ActionResult ListaPrazosDaEquipe(ProcessoVO pesquisa)
        {
            int codSetor = Convert.ToInt32(Session["codUndInterna"]);

            if (!Administrador(codSetor))
            {
                pesquisa.Equipe = Convert.ToInt32(Session["codUndInterna"]);
            }

            var prazos = rControleProcesso.ListaPrazosPorEquipe(pesquisa);

            ViewBag.Equipe = pesquisa.Equipe == 0 ? "Todas as unidades" : rUnidadeInterna.BuscaUnidadeInterna(pesquisa.Equipe).NomeUnidadeInterna;

            ViewBag.ExibirGrafico = false;

            if (prazos.Count > 0)
            {
                GraficoPrazosEquipe(pesquisa);
                ViewBag.ExibirGrafico = true;
            }

            return View(prazos);
        }
        public ActionResult PartialListaPrazosDaEquipe()
        {
            IEnumerable<VW_CotroleDeProcessos> lista = new List<VW_CotroleDeProcessos>();
            return View(lista);
        }
        [HttpPost]
        public ActionResult PartialListaPrazosDaEquipe(ProcessoVO pesquisa)
        {
            var lista = rControleProcesso.ListaPrazosPorEquipe(pesquisa);
            return PartialView(lista);
        }
        public ActionResult PrazoGeral()
        {
            int[] totais = new ControleDeProcessosRepositorio().ListaPrazosGeral();
            return View(Chart(totais, "Situação dos Processos Referente aos Prazos"));
        }
        public ActionResult GraficoPrazosEquipe(ProcessoVO pesquisa)
        {
            var lista = rControleProcesso.ListaPrazosPorEquipe(pesquisa);

            int atrasado = 0;
            int quaseNoPrazo = 0;
            int noPrazo = 0;
            int total = 0;

            foreach (var item in lista)
            {

                if (item.TotalDias > item.LimiteDeDias)
                {
                    atrasado++;
                }
                else if ((item.TotalDias + 7) > item.LimiteDeDias)
                {
                    quaseNoPrazo++;
                }
                else
                {
                    noPrazo++;
                }

            }

            total = atrasado + quaseNoPrazo + noPrazo;
            int[] totais = new int[4];
            totais[0] = atrasado;
            totais[1] = quaseNoPrazo;
            totais[2] = noPrazo;
            totais[3] = total;

            Highcharts grafico = ChartEquipe(totais, "Processos da Equipe");

            return View(grafico);

        }
        protected Highcharts Chart(int[] totais, string titulo)
        {

            ViewBag.Totais = totais;
            Highcharts chart = new Highcharts("chart")                
                .InitChart(new Chart
                {                    
                    Type = ChartTypes.Pie,
                    MarginTop = 80,
                    MarginRight = 40,
                    Options3d = new ChartOptions3d
                    {
                        Enabled = true,
                        Alpha = 45,
                        Beta = 0
                    }
                })
                .SetOptions(new GlobalOptions
                {
                    Colors = new[]
                                         {
                                             ColorTranslator.FromHtml("#b84d45"),
                                             ColorTranslator.FromHtml("#f0ad4e"),
                                             ColorTranslator.FromHtml("#5cb85c"),
                                             ColorTranslator.FromHtml("#7CB5EC")
                                         }
                })
                .SetTitle(new Title { Text = titulo })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.percentage:.1f}%</b>" })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {                      
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        Depth = 35,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Format = "{point.name}"
                        }

                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Prazos",
                    Data = new Data(new object[]
                    {
                        new object[] { "Atrasados", totais[0] },
                        new object[] { "Chegando no Prazo", totais[1] },
                        new Point
                        {
                            Name = "No Prazo",
                            Y = totais[2],
                            Sliced = true,
                            Selected = true
                        },
                        new object[] { "Concluidos", totais[3] }
                    })
                });
            return chart;
        }
        protected Highcharts ChartEquipe(int[] totais, string titulo)
        {

            ViewBag.Totais = totais;
            Highcharts chart = new Highcharts("chart")
                .InitChart(new Chart
                {
                    Type = ChartTypes.Pie,
                    MarginTop = 80,
                    MarginRight = 40,
                    Options3d = new ChartOptions3d
                    {
                        Enabled = true,
                        Alpha = 45,
                        Beta = 0
                    }
                })
                .SetOptions(new GlobalOptions
                {
                    Colors = new[]
                                         {
                                             ColorTranslator.FromHtml("#b84d45"),                                             
                                             ColorTranslator.FromHtml("#f0ad4e"),
                                             ColorTranslator.FromHtml("#5cb85c")                                             
                                         }
                })
                .SetTitle(new Title { Text = titulo })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.percentage:.1f}%</b>" })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        Depth = 35,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Format = "{point.name}"
                        }

                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Prazos",
                    Data = new Data(new object[]
                    {
                        new object[] { "Atrasados", totais[0] },
                        new object[] { "Chegando no Prazo", totais[1] },
                        new Point
                        {
                            Name = "No Prazo",
                            Y = totais[2],
                            Sliced = true,
                            Selected = true                            
                        }
                    })
                });
            return chart;
        }
        protected IEnumerable<object> ListaAnos()
        {
            List<SelectListItem> Anos = new List<SelectListItem>();

            for (int i = 0; i < 3; i++)
            {
                int ano = (DateTime.Now.Year - 1) + i;
                string anotext = ano.ToString();
                Anos.Add(new SelectListItem { Text = anotext, Value = anotext.ToString() });
            }

            return Anos;            
        }
        protected bool Administrador(int codSetor)
        {
            bool permitido = false;
            if (codSetor == 3 || codSetor == 4 || codSetor == 8)
            {
                permitido = true;
            }
            return permitido;
        }
        public ActionResult Destaque() 
        {
            var processos = new ProcessoRepositorio().ProcessosEmDestaque();
            return View(processos);
        }

        public ActionResult BuscaProcessoDestaque(string id) 
        {
            TB_Processo processo = new TB_Processo();// ProcessoRepositorio().BuscarProcesso(id);
            processo.NumeroProcesso = id;
            return RedirectToAction("Historico", processo);
        }

    }
}