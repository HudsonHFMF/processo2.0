namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_TipoParticipacao")]
    public partial class TB_TipoParticipacao
    {
        public TB_TipoParticipacao()
        {
            TB_EmpresaParticipante = new HashSet<TB_EmpresaParticipante>();
        }

        [Key]
        public int CodTipoParticipacao { get; set; }

        [Required]
        [StringLength(50)]
        public string DescricaoParticipacao { get; set; }

        public virtual ICollection<TB_EmpresaParticipante> TB_EmpresaParticipante { get; set; }
    }
}
