namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_ResultadoLicitacao")]
    public partial class TB_ResultadoLicitacao
    {
        [Key]
        public int CodResultado { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        public int CodigoParticipante { get; set; }

        [StringLength(500)]
        public string Item { get; set; }

        [Column(TypeName = "money")]
        public decimal ValorEstimado { get; set; }

        [Column(TypeName = "money")]
        public decimal ValorObtido { get; set; }

        public float Diferenca { get; set; }

        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string UsuarioCadastro { get; set; }

        public DateTime? DataAlteracao { get; set; }

        [StringLength(100)]
        public string UsuarioAlteracao { get; set; }

        public virtual TB_EmpresaParticipante TB_EmpresaParticipante { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }
    }
}
