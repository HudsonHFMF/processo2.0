namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_Recurso")]
    public partial class TB_Recurso
    {
        [Key]
        public int CodRecurso { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        public int CodParticipante { get; set; }

        public bool? Aceito { get; set; }

        public bool? Rejeitado { get; set; }

        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string UsuarioCadastro { get; set; }

        public DateTime? DataAlteracao { get; set; }

        [StringLength(100)]
        public string UsuarioAlteracao { get; set; }

        public virtual TB_EmpresaParticipante TB_EmpresaParticipante { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }
    }
}
