namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_TipoMovimento")]
    public partial class TB_TipoMovimento
    {
        public TB_TipoMovimento()
        {
            TB_MovimentoProcesso = new HashSet<MovimentoProcesso>();
        }

        [Key]
        public short CodTipoMovimento { get; set; }

        [StringLength(70)]
        public string NomeMovimento { get; set; }

        public virtual ICollection<MovimentoProcesso> TB_MovimentoProcesso { get; set; }
    }
}
