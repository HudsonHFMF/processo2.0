namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_Investida")]
    public partial class TB_Investida
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoInvestida { get; set; }

        [StringLength(100)]
        public string TipoInvestida { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroProcessoInvestido { get; set; }

        public DateTime? DataSuspensaoAnulacao { get; set; }

        public int? DiasComunicacao { get; set; }

        public DateTime? DataLimiteComunicacao { get; set; }

        public bool? ComunicadoNoPeriodo { get; set; }

        [StringLength(150)]
        public string DecisaoSuspensaoAnulacao { get; set; }

        public DateTime? DataDecisaoSuspensaoAnulacao { get; set; }

        [StringLength(50)]
        public string NumeroProcessoTribunalSuspensao { get; set; }

        public DateTime? DataComunicadoTribunal { get; set; }

        [StringLength(150)]
        public string NumeroDocumento { get; set; }

        public DateTime? DataComunicadoSecretaria { get; set; }

        [StringLength(10)]
        public string NumeroDocumentoSecretaria { get; set; }

        public DateTime? DataLiberacao { get; set; }

        [StringLength(150)]
        public string DecisaoLiberacao { get; set; }

        [StringLength(50)]
        public string NumeroProcessoTribunalLiberacao { get; set; }

        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string UsuarioCadastro { get; set; }

        public DateTime? DataEdicao { get; set; }

        [StringLength(100)]
        public string UsuarioEdicao { get; set; }

        public bool? InvestidaConcluida { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }
    }
}
