namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_ModeloDespacho")]
    public partial class TB_ModeloDespacho
    {
        public TB_ModeloDespacho()
        {
            TB_Processo = new HashSet<TB_Processo>();
        }

        [Key]
        public int CodModelo { get; set; }

        [StringLength(100)]
        public string NomeRelatorio { get; set; }

        [StringLength(100)]
        public string TipoDespacho { get; set; }

        public bool? Ativo { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo { get; set; }
    }
}
