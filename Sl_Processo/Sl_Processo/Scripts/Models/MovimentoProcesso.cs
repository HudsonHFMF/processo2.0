namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_MovimentoProcesso")]
    public class MovimentoProcesso
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodMovimento { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string CodProcesso { get; set; }

        public short CodUnidadeOrigem { get; set; }

        public short CodUnidadeDestino { get; set; }

        [DataType(DataType.Date)]
        public DateTime DataMovimento { get; set; }

        public short? CodTipoMovimento { get; set; }

        [StringLength(700)]
        public string ObservacaoMovimento { get; set; }

        public int? CodDescricao { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string Usuario { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DataRecebimento { get; set; }

        [StringLength(100)]
        public string UsuarioRec { get; set; }

        public virtual TB_DescricaoMovimento TB_DescricaoMovimento { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }

        public virtual TB_TipoMovimento TB_TipoMovimento { get; set; }

        public virtual TB_UnidadeInterna TB_UnidadeInterna { get; set; }

        public virtual TB_UnidadeInterna TB_UnidadeInterna1 { get; set; }
    }
}
