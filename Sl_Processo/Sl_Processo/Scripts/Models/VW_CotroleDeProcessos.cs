namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VW_CotroleDeProcessos
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        [StringLength(100)]
        public string Objeto { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Sigla { get; set; }

        [Key]
        [Column(Order = 2, TypeName = "smalldatetime")]
        public DateTime DataMovimento { get; set; }

        public short? CodTipoMovimento { get; set; }

        [StringLength(75)]
        public string NomeUnidadeInterna { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodMovimento { get; set; }

        public int? TotalDias { get; set; }

        public int? Ano { get; set; }

        public int? LimiteDeDias { get; set; }

        public bool? ativo { get; set; }

        [StringLength(150)]
        public string DescricaoMovimento { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodDescricaoMovimento { get; set; }

        public short? SetorAtual { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEstimado { get; set; }

        [StringLength(700)]
        public string ObservacaoMovimento { get; set; }

        [StringLength(50)]
        public string NumeroLicitacao { get; set; }

        public int? CodModalidade { get; set; }

        public short CodTipoSituacao { get; set; }
    }
}
