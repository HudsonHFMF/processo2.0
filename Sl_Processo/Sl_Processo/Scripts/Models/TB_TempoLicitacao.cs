namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_TempoLicitacao")]
    public partial class TB_TempoLicitacao
    {
        [Key]
        public int CodTempo { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        public DateTime DataInicio { get; set; }

        [Required]
        public string AtividadeRealizada { get; set; }

        public DateTime DataFim { get; set; }

        public int TotalDias { get; set; }

        public DateTime? DataCadastro { get; set; }

        [StringLength(150)]
        public string UsuarioCadastro { get; set; }

        public DateTime? DataAlteracao { get; set; }

        [StringLength(150)]
        public string UsuarioAlteracao { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }
    }
}
