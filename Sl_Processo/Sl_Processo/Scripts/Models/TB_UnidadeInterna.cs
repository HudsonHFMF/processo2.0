namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_UnidadeInterna")]
    public partial class TB_UnidadeInterna
    {
        public TB_UnidadeInterna()
        {
            TB_PregoeiroUnidade = new HashSet<TB_PregoeiroUnidade>();
            TB_MovimentoProcesso = new HashSet<MovimentoProcesso>();
            TB_MovimentoProcesso1 = new HashSet<MovimentoProcesso>();
            TB_Processo = new HashSet<TB_Processo>();
            TB_Processo1 = new HashSet<TB_Processo>();
            TB_Usuarios = new HashSet<TB_Usuarios>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short CodUnidadeInterna { get; set; }

        [StringLength(75)]
        public string NomeUnidadeInterna { get; set; }

        public virtual ICollection<TB_PregoeiroUnidade> TB_PregoeiroUnidade { get; set; }

        public virtual ICollection<MovimentoProcesso> TB_MovimentoProcesso { get; set; }

        public virtual ICollection<MovimentoProcesso> TB_MovimentoProcesso1 { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo1 { get; set; }

        public virtual ICollection<TB_Usuarios> TB_Usuarios { get; set; }
    }
}
