namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_SistemaCompras")]
    public partial class TB_SistemaCompras
    {
        public TB_SistemaCompras()
        {
            TB_Processo = new HashSet<TB_Processo>();
        }

        [Key]
        public short CodSistemaCompras { get; set; }

        [StringLength(100)]
        public string NomeSistemaCompras { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo { get; set; }
    }
}
