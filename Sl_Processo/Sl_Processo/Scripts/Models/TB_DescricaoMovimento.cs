namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_DescricaoMovimento")]
    public partial class TB_DescricaoMovimento
    {
        public TB_DescricaoMovimento()
        {
            TB_MovimentoProcesso = new HashSet<MovimentoProcesso>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodDescricaoMovimento { get; set; }

        [StringLength(150)]
        public string DescricaoMovimento { get; set; }

        public int? LimiteDeDias { get; set; }

        public bool? ativo { get; set; }

        [Column(TypeName = "text")]
        public string detalheDaAtribuicao { get; set; }

        public virtual ICollection<MovimentoProcesso> TB_MovimentoProcesso { get; set; }
    }
}
