﻿namespace Sl_Processo.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("processos.VW_ResultadoPrincipalLicitacao")]
    public partial class VW_ResultadoPrincipalLicitacao
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        [StringLength(75)]
        public string Comissao { get; set; }
              

        [Key]
        [Column(Order = 1)]
        [StringLength(200)]
        public string TipoProcedimento { get; set; }

        [StringLength(50)]
        public string NumeroProcedimento { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Sigla { get; set; }

        [StringLength(500)]
        public string Objeto { get; set; }

        public int Mes { get; set; }

        public int Ano { get; set; }

        [StringLength(100)]
        public string Situacao { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEstimado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorAdjudicado { get; set; }

        public int? QtdTotalEmpresaVencedor { get; set; }

        public int? QtdVencedorRO { get; set; }

        public int? QtdVencedorROEPPME { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValAdjudicadoEmpresaRO { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValAdjudicadoEmpresaROEPPME { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValAdjudicadoEmpresaEPPME { get; set; }

        public int? TempoLicitacao { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short CodUnidadeInterna { get; set; }
    }
}