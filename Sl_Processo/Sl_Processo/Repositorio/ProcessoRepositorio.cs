﻿using Sl_Processo.Models;
using Sl_Processo.Models.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Collections;

namespace Sl_Processo.Repositorio
{
    public class ProcessoRepositorio
    {
        private readonly Context _Db = new Context();

        public TB_Processo BuscarProcessoPorNumeroProcesso(TB_Processo processo)
        {
            //var lista = new List<MovimentoProcesso>();
            //lista = _Db.MovimentoProcesso.Where(m => m.CodProcesso == processo.NumeroProcesso && m.CodTipoMovimento != 1).ToList();

            var qry = (from m in _Db.MovimentoProcesso
                              where m.CodProcesso == processo.NumeroProcesso && m.CodTipoMovimento != 1 orderby m.DataMovimento descending
                              select m);

            
            var novoProcesso = _Db.Processo.Include(p => p.TB_MovimentoProcesso).Where(p => p.NumeroProcesso == processo.NumeroProcesso).FirstOrDefault();

            novoProcesso.TB_MovimentoProcesso =qry.ToList();

            return novoProcesso;
        }
        public IEnumerable<TB_Processo> BuscarProcessoPorAssunto(TB_Processo processo)
        {
            var qry = _Db.Processo.Where(p => p.Objeto.Contains(processo.Objeto) && p.NumeroProcesso.ToString().Length > 0).ToList();
            
            if (qry.Count == 0)
            {
                List<TB_Processo> listaDeResultados = new List<TB_Processo>();
                string[] palavra = processo.Objeto.Split();

                foreach (string item in palavra)
                {
                    listaDeResultados.AddRange(_Db.Processo.Where(p => p.Objeto.Contains(" " + item + " ")).ToList());
                }

                return listaDeResultados;
                
            }
            else
            {
                return qry;
            }
        }
        public IEnumerable<TB_Processo> BuscarTodos()
        {
            return _Db.Processo;
        }
        public IEnumerable<TB_UnidadeInterna> ListaEquipes()
        {
            return _Db.UnidadeInterna.ToList().OrderBy(i => i.NomeUnidadeInterna);
        }
        public IEnumerable<TB_UnidadeInterna> BuscarUnidadePorId(int codUnidade)
        {
            return _Db.UnidadeInterna.Where(u => u.CodUnidadeInterna == codUnidade);
        }
        public IEnumerable<TB_DescricaoMovimento> ListaDescricaoMovimentacao()
        {
            return _Db.DescricaoMovimento.Where(i => i.ativo == true).OrderBy(i => i.DescricaoMovimento).ToList();

        }
        public IEnumerable<TB_Processo> BuscarProcessosDoParticipanteNoPeriodo(int codParticipante)
        {
            return _Db.Processo.
                Where(p => p.TB_EmpresaParticipante.
                Any(e => e.CodParticipante == codParticipante)).ToList();
        }
        public IEnumerable<TB_Processo> ProcessosEmDestaque() 
        {
            var processos = _Db.Processo.Where(i => i.Priorizar == true).OrderBy(p => p.NumeroProcesso);
            return processos;
        }
        public TB_Processo BuscarProcesso(string numProcesso) 
        {
            return _Db.Processo.FirstOrDefault(p => p.NumeroProcesso == numProcesso);
        }
    }
}