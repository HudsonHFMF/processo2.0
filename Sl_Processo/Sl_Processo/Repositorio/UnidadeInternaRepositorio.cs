﻿using Sl_Processo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Sl_Processo.Repositorio
{
    public class UnidadeInternaRepositorio
    {
        private readonly Context _Db = new Context();

        public TB_UnidadeInterna BuscaUnidadeInterna(int cod) 
        {
            return _Db.UnidadeInterna.FirstOrDefault(i => i.CodUnidadeInterna == cod);
        }
    }
}