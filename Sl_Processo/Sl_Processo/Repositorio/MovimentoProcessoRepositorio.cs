﻿using Sl_Processo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sl_Processo.Repositorio
{
    public class MovimentoProcessoRepositorio
    {
        private readonly Context _Db = new Context();

        public IEnumerable<MovimentoProcesso> BuscarHistoricoPorNumeroProcesso(TB_Processo processo)
        {
            return _Db.MovimentoProcesso.Where(p => p.CodProcesso == processo.NumeroLicitacao).OrderBy(p=>p.DataMovimento);
        }
        public IEnumerable<MovimentoProcesso> BuscarHistoricoPorConteudo(TB_Processo processo)
        {
            return _Db.MovimentoProcesso.Where(p => p.CodProcesso == processo.NumeroProcesso);
        }
    }
}