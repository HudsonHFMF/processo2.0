﻿using System.Collections.Generic;
using System.Linq;
using Sl_Processo.Models;
using Sl_Processo.Models.ValueObjects;
using System;

namespace Sl_Processo.Repositorio
{
    public class ControleDeProcessosRepositorio
    {
        private readonly Context _Db = new Context();


        public ICollection<VW_CotroleDeProcessos> ListaPrazosPorEquipe(ProcessoVO pesquisa) 
        {
            
            if (pesquisa.Equipe == 0 && pesquisa.Assunto == 0)
            {
                return _Db.VW_CotroleDeProcessos.Where(i => i.Ano == pesquisa.Ano).OrderByDescending(i => i.TotalDias > i.LimiteDeDias).ThenByDescending(i => (i.TotalDias + 7) > i.LimiteDeDias).ToList();
            
            }
            else if (pesquisa.Equipe == 0)
            {
                return _Db.VW_CotroleDeProcessos.Where(i => i.Ano == pesquisa.Ano && i.CodDescricaoMovimento == pesquisa.Assunto).OrderByDescending(i => i.TotalDias > i.LimiteDeDias).ThenByDescending(i => (i.TotalDias + 7) > i.LimiteDeDias).ToList();
            
            }
            else if (pesquisa.Assunto == 0)
            {

                return _Db.VW_CotroleDeProcessos.Where(i => i.Ano == pesquisa.Ano && i.SetorAtual == pesquisa.Equipe).OrderByDescending(i => i.TotalDias > i.LimiteDeDias).ThenByDescending(i => (i.TotalDias + 7) > i.LimiteDeDias).ToList();
            
            }
            else
            {
                return _Db.VW_CotroleDeProcessos.Where(i => i.Ano == pesquisa.Ano && i.SetorAtual == pesquisa.Equipe && i.CodDescricaoMovimento == pesquisa.Assunto).OrderByDescending(i => i.TotalDias > i.LimiteDeDias).ThenByDescending(i => (i.TotalDias + 7) > i.LimiteDeDias).ToList();
            
            }
        }

        public int[] ListaPrazosGeral() 
        {
            int ano = DateTime.Now.Year;
            int[] totais = new int[5];

           
            int atrasado = 0;
            int faltaPouco = 0;
            int noPrazo = 0;
            int totalGeral = 0;
            int concluidos = 0;

            var qry = from i in _Db.VW_CotroleDeProcessos
                      where i.Ano == ano
                      select i;

            foreach (var item in qry)
            {
                //var qryConcluidos = _Db.Processo.Any(p => p.DataCadastro.Value.Year == item.Ano && p.NumeroProcesso == item.NumeroProcesso && p.CodSituacaoAtual == 10 && p.TB_ItemFracassado.Count() == 0);
                if (item.CodTipoSituacao == 10)
                {
                    concluidos = concluidos + 1;
                }
                else
                {
                    if (item.TotalDias > item.LimiteDeDias)
                    {
                        atrasado = atrasado + 1;
                    }
                    else if ((item.TotalDias + 7) > item.LimiteDeDias)
                    {
                        faltaPouco = faltaPouco + 1;
                    }
                    else
                    {
                        noPrazo = noPrazo + 1;
                    }
                }
                
            }

            totalGeral = atrasado + faltaPouco + noPrazo + concluidos;

            totais[0] = atrasado;
            totais[1] = faltaPouco;
            totais[2] = noPrazo;
            totais[3] = concluidos;
            totais[4] = totalGeral;
          
            return totais;
        }

        public bool ProcessoConcluido(int? ano, string numeroProcesso) 
        { 
           return _Db.Processo.Any(p => p.DataCadastro.Value.Year == ano && p.NumeroProcesso == numeroProcesso && p.CodSituacaoAtual == 10 && p.TB_ItemFracassado.Count() == 0);
        }
    }
}