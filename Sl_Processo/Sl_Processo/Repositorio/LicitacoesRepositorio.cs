﻿using Sl_Processo.Models;
using Sl_Processo.Models.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sl_Processo.Repositorio
{
    public class LicitacoesRepositorio
    {
        private readonly Context _Db = new Context();

        public IEnumerable<VW_ResultadoPrincipalLicitacao> BuscarTodos()
        {
            return _Db.VW_ResultadoPrincipalLicitacao;
        }
        public ICollection<VW_ResultadoPrincipalLicitacao> ListaProcessosCriterio(LicitacaoVO pesquisa)
        {

            if (pesquisa.Equipe == 0 && string.IsNullOrEmpty(pesquisa.Processo))
            {
                return _Db.VW_ResultadoPrincipalLicitacao.Where(i => i.Ano == pesquisa.Ano).ToList();

            }
            else if (pesquisa.Equipe == 0)
            {
                return _Db.VW_ResultadoPrincipalLicitacao.Where(i => i.Ano == pesquisa.Ano && i.NumeroProcesso == pesquisa.Processo).ToList();

            }
            else if (string.IsNullOrEmpty(pesquisa.Processo))
            {

                return _Db.VW_ResultadoPrincipalLicitacao.Where(i => i.Ano == pesquisa.Ano && i.CodUnidadeInterna == pesquisa.Equipe).ToList();

            }
            else
            {
                return _Db.VW_ResultadoPrincipalLicitacao.Where(i => i.Ano == pesquisa.Ano && i.CodUnidadeInterna == pesquisa.Equipe && i.NumeroProcesso == pesquisa.Processo).ToList();

            }
        }


    }
}