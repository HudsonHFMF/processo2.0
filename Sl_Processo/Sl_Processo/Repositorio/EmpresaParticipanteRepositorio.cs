﻿using Sl_Processo.Models;
using Sl_Processo.Models.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sl_Processo.Repositorio
{
    public class EmpresaParticipanteRepositorio
    {
        private readonly Context _Db = new Context();

        public TB_EmpresaParticipante BuscarParticipantePorCodigo(int codParticipante)
        {
            return _Db.EmpresaParticipante.FirstOrDefault(e => e.CodParticipante == codParticipante);
        }
        public IEnumerable<ParticipanteVO> BuscarTodos(string letra)
        {
            var qry = (from item in _Db.EmpresaParticipante
                       where item.RazaoSocial.StartsWith(letra)
                       select new
                       {
                           item.CodParticipante,
                           item.CNPJFormatado,
                           item.RazaoSocial,
                           item.DataCadastro
                       }).GroupBy(i=>i.CNPJFormatado);
            var lista = new List<ParticipanteVO>();

            foreach (var participante in qry)
            {
                var objParticipante = new ParticipanteVO();

                objParticipante.CodEmpresaParticipante = participante.FirstOrDefault().CodParticipante;
                objParticipante.CnpjParticipante = participante.FirstOrDefault().CNPJFormatado;
                objParticipante.RazaoSocialParticipante = participante.FirstOrDefault().RazaoSocial;
                objParticipante.DataCadastro = (DateTime)participante.FirstOrDefault().DataCadastro;
                lista.Add(objParticipante);
            }
            //return _Db.EmpresaParticipante.Where(e => e.RazaoSocial.StartsWith(letra) && e.CNPJFormatado == _Db.EmpresaParticipante.FirstOrDefault(c=>c.CNPJFormatado == e.CNPJFormatado).CNPJFormatado).OrderBy(e=>e.RazaoSocial);
            return lista;
        }
    }
}