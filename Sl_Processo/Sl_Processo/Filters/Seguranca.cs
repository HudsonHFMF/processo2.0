﻿using System.Web.Mvc.Filters;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sl_Processo.Filters
{
    public class Seguranca : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var logado = filterContext.HttpContext.Session["user"];
            var usuario = filterContext.HttpContext.Session["user"];
            if (usuario == null || usuario != logado)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller = "Home", Action = "Index" }));
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            var logado = filterContext.HttpContext.Session["user"];
            var usuario = filterContext.HttpContext.Session["user"];
            if (usuario == null || usuario != logado)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller = "Home", Action = "Index" }));
            }
        }
    }
}