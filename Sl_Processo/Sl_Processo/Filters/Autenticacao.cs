﻿using Sl_Processo.Models;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace Sl_Processo.Filters
{
    public class Autenticacao: Seguranca
    {        
        
        public static bool AutenticarUsuario(string login, string loginsenha)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Modelo"].ConnectionString);
            con.Open();
            string chave = login + loginsenha;
            SqlCommand com = new SqlCommand("Select susy.comparaSenha('" + login + "','" + chave + "')", con);

         
            return (Boolean)com.ExecuteScalar();
        }

        public static TB_Usuarios BuscarUsuario(string login, string senha)
        {
            Context model = new Context();

            if (AutenticarUsuario(login,senha))
            {
                return model.Usuario.FirstOrDefault(i => i.login == login);
            }
            else
            {
                return null;
            }

                    
        }
    }
}