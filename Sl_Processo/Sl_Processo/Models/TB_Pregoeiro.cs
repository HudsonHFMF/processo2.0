namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("geral.TB_Pregoeiro")]
    public partial class TB_Pregoeiro
    {
        public TB_Pregoeiro()
        {
            TB_PregoeiroUnidade = new HashSet<TB_PregoeiroUnidade>();
            TB_Processo = new HashSet<TB_Processo>();
        }

        [Key]
        public int CodPregoeiro { get; set; }

        [StringLength(300)]
        public string NomePregoeiro { get; set; }

        public bool? Ativo { get; set; }

        public int? Matricula { get; set; }

        [StringLength(100)]
        public string Descricao { get; set; }

        public virtual ICollection<TB_PregoeiroUnidade> TB_PregoeiroUnidade { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo { get; set; }
    }
}
