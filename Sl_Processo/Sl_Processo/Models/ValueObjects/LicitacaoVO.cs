﻿namespace Sl_Processo.Models.ValueObjects
{
    public class LicitacaoVO
    {
        public int Ano { get; set; }
        public int Equipe { get; set; }
        public string Processo { get; set; }
    }
}