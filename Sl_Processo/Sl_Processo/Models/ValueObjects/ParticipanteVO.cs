﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sl_Processo.Models.ValueObjects
{
    public class ParticipanteVO
    {
        public int CodEmpresaParticipante { get; set; }
        public string CnpjParticipante { get; set; }
        public string RazaoSocialParticipante { get; set; }
        public DateTime PeriodoInicio { get; set; }
        public DateTime PeriodoFim { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}