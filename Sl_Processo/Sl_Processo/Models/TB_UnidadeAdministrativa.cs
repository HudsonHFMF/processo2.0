namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("geral.TB_UnidadeAdministrativa")]
    public partial class TB_UnidadeAdministrativa
    {
        public TB_UnidadeAdministrativa()
        {
            TB_Processo = new HashSet<TB_Processo>();
            TB_Usuarios = new HashSet<TB_Usuarios>();
        }

        [Key]
        public int CodUnidadeAdministrativa { get; set; }

        [Required]
        [StringLength(400)]
        public string NomeUnidadeAdministrativa { get; set; }

        [Required]
        [StringLength(50)]
        public string Sigla { get; set; }

        [StringLength(500)]
        public string Endereco { get; set; }

        [StringLength(50)]
        public string Telefone { get; set; }

        public bool? OrgaoEx { get; set; }

        public int? idEsferaAdministrativa { get; set; }

        [StringLength(6)]
        public string idMunicipio { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo { get; set; }

        public virtual ICollection<TB_Usuarios> TB_Usuarios { get; set; }
    }
}
