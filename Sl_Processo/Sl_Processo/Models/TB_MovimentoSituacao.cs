namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_MovimentoSituacao")]
    public partial class TB_MovimentoSituacao
    {
        [Key]
        public int CodMovimentoSituacao { get; set; }

        [StringLength(50)]
        public string CodProcesso { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataSituacao { get; set; }

        public short? CodTipo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string Usuario { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }

        public virtual TB_TipoSituacao TB_TipoSituacao { get; set; }
    }
}
