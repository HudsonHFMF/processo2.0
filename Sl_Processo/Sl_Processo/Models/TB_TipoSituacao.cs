namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_TipoSituacao")]
    public partial class TB_TipoSituacao
    {
        public TB_TipoSituacao()
        {
            TB_MovimentoSituacao = new HashSet<TB_MovimentoSituacao>();
            TB_Processo = new HashSet<TB_Processo>();
        }

        [Key]
        public short CodTipoSituacao { get; set; }

        [StringLength(100)]
        public string CodNomeSituacao { get; set; }

        public virtual ICollection<TB_MovimentoSituacao> TB_MovimentoSituacao { get; set; }

        public virtual ICollection<TB_Processo> TB_Processo { get; set; }
    }
}
