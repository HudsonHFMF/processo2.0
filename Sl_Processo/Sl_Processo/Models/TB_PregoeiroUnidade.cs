namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("geral.TB_PregoeiroUnidade")]
    public partial class TB_PregoeiroUnidade
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodPregoeiro { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short CodUnidade { get; set; }

        public bool? Ativo { get; set; }

        public virtual TB_Pregoeiro TB_Pregoeiro { get; set; }

        public virtual TB_UnidadeInterna TB_UnidadeInterna { get; set; }
    }
}
