namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_Processo")]
    public partial class TB_Processo
    {
        public TB_Processo()
        {
            TB_EmpresaParticipante = new HashSet<TB_EmpresaParticipante>();
            TB_Impugnacao = new HashSet<TB_Impugnacao>();
            TB_Investida = new HashSet<TB_Investida>();
            TB_ItemFracassado = new HashSet<TB_ItemFracassado>();
            TB_MovimentoProcesso = new HashSet<MovimentoProcesso>();
            TB_MovimentoSituacao = new HashSet<TB_MovimentoSituacao>();
            TB_Recurso = new HashSet<TB_Recurso>();
            TB_ResultadoLicitacao = new HashSet<TB_ResultadoLicitacao>();
            TB_TempoLicitacao = new HashSet<TB_TempoLicitacao>();
        }

        [Key]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        public int? CodUnidadeAdministrativa { get; set; }

        public bool? OutroProcesso { get; set; }

        public short? CodCPL { get; set; }

        public int? CodPregoeiro { get; set; }

        [StringLength(500)]
        public string Objeto { get; set; }

        [StringLength(50)]
        public string NumeroLicitacao { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataHoraLicitacao { get; set; }

        public int? CodModalidade { get; set; }

        public bool? AnaliseTCE { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataRetiradaTCE { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEstimado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorLicitado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorFracassado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEconomizado { get; set; }

        public short? CodSistemaCompras { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataEntrada { get; set; }

        public bool? Concluido { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string Usuario { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DataEdicao { get; set; }

        [StringLength(100)]
        public string UsuarioEdicao { get; set; }

        public short? SetorAtual { get; set; }

        public short? CodSituacaoAtual { get; set; }

        public int? CodModeloDespacho { get; set; }

        public DateTime? DataDespacho { get; set; }

        public string ObservacaoDespacho { get; set; }

        public DateTime? DataEmissaoDespachoFinal { get; set; }

        public string ObservacaoFinalPregoeiro { get; set; }

        public bool Bloqueado { get; set; }

        public bool? Priorizar { get; set; }

        public virtual TB_Pregoeiro TB_Pregoeiro { get; set; }

        public virtual TB_UnidadeAdministrativa TB_UnidadeAdministrativa { get; set; }

        public virtual ICollection<TB_EmpresaParticipante> TB_EmpresaParticipante { get; set; }

        public virtual ICollection<TB_Impugnacao> TB_Impugnacao { get; set; }

        public virtual ICollection<TB_Investida> TB_Investida { get; set; }

        public virtual ICollection<TB_ItemFracassado> TB_ItemFracassado { get; set; }

        public virtual TB_ModeloDespacho TB_ModeloDespacho { get; set; }

        public virtual ICollection<MovimentoProcesso> TB_MovimentoProcesso { get; set; }

        public virtual ICollection<TB_MovimentoSituacao> TB_MovimentoSituacao { get; set; }

        public virtual TB_SistemaCompras TB_SistemaCompras { get; set; }

        public virtual TB_TipoSituacao TB_TipoSituacao { get; set; }

        public virtual TB_UnidadeInterna TB_UnidadeInterna { get; set; }

        public virtual TB_UnidadeInterna TB_UnidadeInterna1 { get; set; }

        public virtual ICollection<TB_Recurso> TB_Recurso { get; set; }

        public virtual ICollection<TB_ResultadoLicitacao> TB_ResultadoLicitacao { get; set; }

        public virtual ICollection<TB_TempoLicitacao> TB_TempoLicitacao { get; set; }
    }
}
