namespace Sl_Processo.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Modelo")
        {

        }

        public virtual DbSet<TB_Pregoeiro> Pregoeiro { get; set; }
        public virtual DbSet<TB_PregoeiroUnidade> PregoeiroUnidade { get; set; }
        public virtual DbSet<TB_UnidadeAdministrativa> UnidadeAdministrativa { get; set; }
        public virtual DbSet<TB_DescricaoMovimento> DescricaoMovimento { get; set; }
        public virtual DbSet<TB_EmpresaParticipante> EmpresaParticipante { get; set; }
        public virtual DbSet<TB_Impugnacao> Impugnacao { get; set; }
        public virtual DbSet<TB_Investida> Investida { get; set; }
        public virtual DbSet<TB_ItemFracassado> ItemFracassado { get; set; }
        public virtual DbSet<TB_ModeloDespacho> ModeloDespacho { get; set; }
        public DbSet<MovimentoProcesso> MovimentoProcesso { get; set; }
        public virtual DbSet<TB_MovimentoSituacao> MovimentoSituacao { get; set; }
        public virtual DbSet<TB_Processo> Processo { get; set; }
        public virtual DbSet<TB_Recurso> Recurso { get; set; }
        public virtual DbSet<TB_ResultadoLicitacao> ResultadoLicitacao { get; set; }
        public virtual DbSet<TB_SistemaCompras> SistemaCompras { get; set; }
        public virtual DbSet<TB_TempoLicitacao> TempoLicitacao { get; set; }
        public virtual DbSet<TB_TipoMovimento> TipoMovimento { get; set; }
        public virtual DbSet<TB_TipoParticipacao> TipoParticipacao { get; set; }
        public virtual DbSet<TB_TipoSituacao> TipoSituacao { get; set; }
        public virtual DbSet<TB_UnidadeInterna> UnidadeInterna { get; set; }
        public virtual DbSet<TB_Usuarios> Usuario { get; set; }
        public virtual DbSet<VW_CotroleDeProcessos> VW_CotroleDeProcessos { get; set; }
        public virtual DbSet<VW_ResultadoPrincipalLicitacao> VW_ResultadoPrincipalLicitacao { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TB_Pregoeiro>()
                .HasMany(e => e.TB_PregoeiroUnidade)
                .WithRequired(e => e.TB_Pregoeiro)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_UnidadeAdministrativa>()
                .HasMany(e => e.TB_Usuarios)
                .WithOptional(e => e.TB_UnidadeAdministrativa)
                .HasForeignKey(e => e.unidadeAdministrativa);

            modelBuilder.Entity<TB_DescricaoMovimento>()
                .Property(e => e.DescricaoMovimento)
                .IsUnicode(false);

            modelBuilder.Entity<TB_DescricaoMovimento>()
                .Property(e => e.detalheDaAtribuicao)
                .IsUnicode(false);

            modelBuilder.Entity<TB_DescricaoMovimento>()
                .HasMany(e => e.TB_MovimentoProcesso)
                .WithOptional(e => e.TB_DescricaoMovimento)
                .HasForeignKey(e => e.CodDescricao);

            modelBuilder.Entity<TB_EmpresaParticipante>()
                .Property(e => e.CNPJFormatado)
                .IsUnicode(false);

            modelBuilder.Entity<TB_EmpresaParticipante>()
                .HasMany(e => e.TB_ResultadoLicitacao)
                .WithRequired(e => e.TB_EmpresaParticipante)
                .HasForeignKey(e => e.CodigoParticipante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Investida>()
                .Property(e => e.NumeroDocumentoSecretaria)
                .IsFixedLength();

            modelBuilder.Entity<TB_ModeloDespacho>()
                .HasMany(e => e.TB_Processo)
                .WithOptional(e => e.TB_ModeloDespacho)
                .HasForeignKey(e => e.CodModeloDespacho);

            modelBuilder.Entity<TB_Processo>()
                .Property(e => e.ValorEstimado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TB_Processo>()
                .Property(e => e.ValorLicitado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TB_Processo>()
                .Property(e => e.ValorFracassado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TB_Processo>()
                .Property(e => e.ValorEconomizado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_EmpresaParticipante)
                .WithRequired(e => e.TB_Processo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_Impugnacao)
                .WithRequired(e => e.TB_Processo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_Investida)
                .WithRequired(e => e.TB_Processo)
                .HasForeignKey(e => e.NumeroProcessoInvestido)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_ItemFracassado)
                .WithRequired(e => e.TB_Processo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_MovimentoProcesso)
                .WithRequired(e => e.TB_Processo)
                .HasForeignKey(e => e.CodProcesso);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_MovimentoSituacao)
                .WithOptional(e => e.TB_Processo)
                .HasForeignKey(e => e.CodProcesso)
                .WillCascadeOnDelete();

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_Recurso)
                .WithRequired(e => e.TB_Processo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_ResultadoLicitacao)
                .WithRequired(e => e.TB_Processo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_Processo>()
                .HasMany(e => e.TB_TempoLicitacao)
                .WithRequired(e => e.TB_Processo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_ResultadoLicitacao>()
                .Property(e => e.Item)
                .IsUnicode(false);

            modelBuilder.Entity<TB_ResultadoLicitacao>()
                .Property(e => e.ValorEstimado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TB_ResultadoLicitacao>()
                .Property(e => e.ValorObtido)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TB_TipoParticipacao>()
                .HasMany(e => e.TB_EmpresaParticipante)
                .WithRequired(e => e.TB_TipoParticipacao)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_TipoSituacao>()
                .HasMany(e => e.TB_MovimentoSituacao)
                .WithOptional(e => e.TB_TipoSituacao)
                .HasForeignKey(e => e.CodTipo);

            modelBuilder.Entity<TB_TipoSituacao>()
                .HasMany(e => e.TB_Processo)
                .WithOptional(e => e.TB_TipoSituacao)
                .HasForeignKey(e => e.CodSituacaoAtual);

            modelBuilder.Entity<TB_UnidadeInterna>()
                .HasMany(e => e.TB_PregoeiroUnidade)
                .WithRequired(e => e.TB_UnidadeInterna)
                .HasForeignKey(e => e.CodUnidade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_UnidadeInterna>()
                .HasMany(e => e.TB_MovimentoProcesso)
                .WithRequired(e => e.TB_UnidadeInterna)
                .HasForeignKey(e => e.CodUnidadeOrigem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_UnidadeInterna>()
                .HasMany(e => e.TB_MovimentoProcesso1)
                .WithRequired(e => e.TB_UnidadeInterna1)
                .HasForeignKey(e => e.CodUnidadeDestino)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TB_UnidadeInterna>()
                .HasMany(e => e.TB_Processo)
                .WithOptional(e => e.TB_UnidadeInterna)
                .HasForeignKey(e => e.CodCPL);

            modelBuilder.Entity<TB_UnidadeInterna>()
                .HasMany(e => e.TB_Processo1)
                .WithOptional(e => e.TB_UnidadeInterna1)
                .HasForeignKey(e => e.SetorAtual);

            modelBuilder.Entity<TB_UnidadeInterna>()
                .HasMany(e => e.TB_Usuarios)
                .WithOptional(e => e.TB_UnidadeInterna)
                .HasForeignKey(e => e.unidadeInterna);

            modelBuilder.Entity<TB_Usuarios>()
                .Property(e => e.nome)
                .IsUnicode(false);

            modelBuilder.Entity<TB_Usuarios>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<TB_Usuarios>()
                .Property(e => e.senha)
                .IsUnicode(false);

            modelBuilder.Entity<VW_CotroleDeProcessos>()
                .Property(e => e.DescricaoMovimento)
                .IsUnicode(false);

            modelBuilder.Entity<VW_CotroleDeProcessos>()
                .Property(e => e.ValorEstimado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<VW_ResultadoPrincipalLicitacao>()
                .Property(e => e.ValorEstimado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<VW_ResultadoPrincipalLicitacao>()
                .Property(e => e.ValorAdjudicado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<VW_ResultadoPrincipalLicitacao>()
                .Property(e => e.ValAdjudicadoEmpresaRO)
                .HasPrecision(19, 4);

            modelBuilder.Entity<VW_ResultadoPrincipalLicitacao>()
                .Property(e => e.ValAdjudicadoEmpresaROEPPME)
                .HasPrecision(19, 4);

            modelBuilder.Entity<VW_ResultadoPrincipalLicitacao>()
                .Property(e => e.ValAdjudicadoEmpresaEPPME)
                .HasPrecision(19, 4);
        }
    }
}
