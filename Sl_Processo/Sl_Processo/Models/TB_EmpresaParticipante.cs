namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_EmpresaParticipante")]
    public partial class TB_EmpresaParticipante
    {
        public TB_EmpresaParticipante()
        {
            TB_Recurso = new HashSet<TB_Recurso>();
            TB_ResultadoLicitacao = new HashSet<TB_ResultadoLicitacao>();
        }

        [Key]
        public int CodParticipante { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        public int CodTipoParticipacao { get; set; }

        [Required]
        [StringLength(20)]
        public string CNPJFormatado { get; set; }

        [Required]
        [StringLength(200)]
        public string RazaoSocial { get; set; }

        public bool? RO { get; set; }

        public bool? EPPME { get; set; }

        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string UsuarioCadastro { get; set; }

        public DateTime? DataAlteracao { get; set; }

        [StringLength(100)]
        public string UsuarioAlteracao { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }

        public virtual TB_TipoParticipacao TB_TipoParticipacao { get; set; }

        public virtual ICollection<TB_Recurso> TB_Recurso { get; set; }

        public virtual ICollection<TB_ResultadoLicitacao> TB_ResultadoLicitacao { get; set; }
    }
}
