namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Core.Objects.DataClasses;
    using System.Data.Entity.Spatial;

    [Table("susy.TB_Usuarios")]
    public partial class TB_Usuarios
    {
        [Key]
        public int usuarioID { get; set; }

        [StringLength(50)]
        public string nome { get; set; }

        public int? unidadeAdministrativa { get; set; }

        public short? unidadeInterna { get; set; }

        [StringLength(100)]
        public string cargo { get; set; }

        [StringLength(50)]
        public string setorSigla { get; set; }

        [StringLength(50)]
        public string telefone { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(30)]
        public string login { get; set; }

        [StringLength(255)]
        public string senha { get; set; }

        public bool? ativo { get; set; }

        public bool? senhaExpirada { get; set; }

        public DateTime? dataCadastro { get; set; }

        public int? usuarioCadastro { get; set; }

        public DateTime? dataAlteracao { get; set; }

        public int? usuarioAlteracao { get; set; }

        public virtual TB_UnidadeAdministrativa TB_UnidadeAdministrativa { get; set; }

        public virtual TB_UnidadeInterna TB_UnidadeInterna { get; set; }

       
        
    }
}
