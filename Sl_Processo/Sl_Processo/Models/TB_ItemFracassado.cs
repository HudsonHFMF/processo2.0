namespace Sl_Processo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processos.TB_ItemFracassado")]
    public partial class TB_ItemFracassado
    {
        [Key]
        public int CodItemFracassado { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroProcesso { get; set; }

        [Required]
        [StringLength(50)]
        public string NumeroItem { get; set; }

        [Required]
        public string Especificacao { get; set; }

        public DateTime? DataCadastro { get; set; }

        [StringLength(100)]
        public string UsuarioCadastro { get; set; }

        public DateTime? DataAlteracao { get; set; }

        [StringLength(100)]
        public string UsuarioAlteracao { get; set; }

        public virtual TB_Processo TB_Processo { get; set; }
    }
}
