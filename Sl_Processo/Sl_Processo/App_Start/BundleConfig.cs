﻿using System.Web;
using System.Web.Optimization;

namespace Sl_Processo
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.maskedinput.js",
                        "~/Scripts/mask.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/datatable1").Include(
                "~/Scripts/datatable1/jquery.js",
                "~/Scripts/datatable1/jquery.dataTables.js",
                "~/Scripts/datatable1/dataTables.buttons.min.js",
                "~/Scripts/datatable1/buttons.colVis.min.js",
                "~/Scripts/datatable1/jszip.min.js",
                "~/Scripts/datatable1/pdfmake.min.js",
                "~/Scripts/datatable1/vs_fonts.js",
                "~/Scripts/datatable1/buttons.html5.min.js",                
                "~/Scripts/datatable1/dataTables.bootstrap.js"));
            
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/font-awesome.css"));

            bundles.Add(new StyleBundle("~/Content/datatable1").Include(
                "~/Content/datatable1/bootstrap.min.css",
                "~/Content/datatable1/dataTables.bootstrap.min.css",
                "~/Content/datatable1/buttons.dataTables.min.css"));
        }
    }
}
